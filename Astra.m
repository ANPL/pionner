function [] = Astra( Image,Depth,pointCloud,pointCloudColor)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if(Image+Depth+pointCloud+pointCloudColor > 0)
Image_Raw=rossubscriber('/camera/rgb/image_raw/compressed');
DepthComp=rossubscriber('/camera/depth/image');
pt=rossubscriber('/camera/depth/points');
ptColor=rossubscriber('/camera/depth_registered/points');

if(Image==1)
    tic;
     while toc<10
  image=rosmessage(Image_Raw);
  image=receive(Image_Raw);
  imshow(readImage(image))
     end
end
if(Depth==1)
   tic;
     while toc<10
    depth=rosmessage(DepthComp);
    depth=receive(DepthComp);
    imshow(readImage(depth))
     end
 
end
if(pointCloudColor==1)
     tic;
     while toc<10
    PCC=rosmessage(ptColor);
    PCC=receive(ptColor);
    scatter3(PCC)
     end
         
end
if(pointCloud==1)
     tic;
     while toc<10
    pic=rosmessage(pt);
    pic=receive(pt);
    scatter3(pic)
     end
end
pointCloud=0;
Image=0;
Depth=0;
pointCloudColor=0;
end
end



