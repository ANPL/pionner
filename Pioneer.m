function [ ] = Pioneer( SonarIsChosen, PoseIsChosen )
%this program allows you to access sonar or odometry data from pionner robot
i=1;
if(PoseIsChosen==1)
    tic;
    while(toc<inf)
    Pose=rossubscriber('/RosAria/pose');
    PoseMsg=rosmessage(Pose);
    PoseMsg=receive(Pose);
    RobotPosition(i,:) = [PoseMsg.Pose.Pose.Position.X PoseMsg.Pose.Pose.Position.Y];
    plot(RobotPosition(:,1),RobotPosition(:,2))
    RobotVelocity(i,:)=[PoseMsg.Twist.Twist.Linear.X,PoseMsg.Twist.Twist.Linear.Y];
    w=PoseMsg.Pose.Pose.Orientation.W;
    z=PoseMsg.Pose.Pose.Orientation.Z;
    y=PoseMsg.Pose.Pose.Orientation.Y;
    x=PoseMsg.Pose.Pose.Orientation.X;
    quat=[w,x,y,z];
    Angles = quat2eul(quat);
    hold on
    quiver(RobotPosition(i,1),RobotPosition(i,2),-sin(Angles(1))*PoseMsg.Twist.Twist.Linear.X,cos(Angles(1))*PoseMsg.Twist.Twist.Linear.X)
    i=i+1;
    end
end
if(SonarIsChosen==1)
    tic;
    while(toc<inf)
    Sonar=rossubscriber('/RosAria/sonar_pointcloud2');
    SonarMsg=rosmessage(Sonar);
    SonarMsg=receive(Sonar);
    Cartez=readXYZ(SonarMsg);
    scatter3(Cartez(:,1),Cartez(:,2),Cartez(:,3))
    end
end
end

