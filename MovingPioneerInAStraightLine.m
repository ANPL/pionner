function [ ] = MovingPioneerInAStraightLine( Distance)
%This function moves the robot forword in a straight line, the value this
%function receives is the distance in meters which the robot would be
%moved.
MaxAllowedError=degtorad(1);
MaxVelocity=1.2;
VelocityPub=rospublisher('/RosAria/cmd_vel');
PositionPub=rossubscriber('/RosAria/pose');
VelocityMsg=rosmessage(VelocityPub);
Position=rosmessage(PositionPub);
Position=receive(PositionPub);
CurrentAngle=QtrToDeg(receive(PositionPub));
CurrentPosition=[Position.Pose.Pose.Position.X,Position.Pose.Pose.Position.Y];
Destination=[CurrentPosition(1)+Distance*cos(CurrentAngle(3)),CurrentPosition(2)+Distance*sin(CurrentAngle(3))]

    while(norm(Destination-CurrentPosition)>0.2)
      
        Position=receive(PositionPub);
        CurrentPosition=[Position.Pose.Pose.Position.X , Position.Pose.Pose.Position.Y];
        whereareyougoing=norm(Destination-CurrentPosition)
        Scale=(norm((Destination)-(CurrentPosition)))/10;
            if(Scale >= MaxVelocity)VelocityMsg.Linear.X=MaxVelocity;
            else VelocityMsg.Linear.X=Scale;
            end
            send(VelocityPub,VelocityMsg);
        % if(abs((CurrentAngle(3)+Angle)-Position.Twist.Twist.Angular.Z)>MaxAllowedError) 
           %  TurningPioneer((CurrentAngle(3)+Angle)-Position.Twist.Twist.Angular.Z); 
          %   k=(CurrentAngle(3)+Angle)-Position.Twist.Twist.Angular.Z
         %end
    end
    
end

