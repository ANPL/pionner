function [ ] = TurningPioneer(Angle)
%This Function Alligns the Robot to the correct angle to the target.
MaxAngularSpeed=degtorad(99);
TurnPub=rospublisher('/RosAria/cmd_vel');
PosePub=rossubscriber('/RosAria/pose');
TurnMsg=rosmessage(TurnPub);
CurrentAngle=QtrToDeg(receive(PosePub));
WantedAngle=Angle+CurrentAngle(3)

if(WantedAngle<-pi)WantedAngle=2*pi+WantedAngle;
end
if(WantedAngle>pi)WantedAngle=WantedAngle-pi*2;
end
while(abs(WantedAngle-CurrentAngle(3))>degtorad(0.5))
CurrentAngle=QtrToDeg(receive(PosePub));
Turn=(WantedAngle-CurrentAngle(3));
if(Turn>pi) Turn=Turn-2*pi;end
if(Turn<-pi)Turn=Turn+2*pi;end
where=radtodeg(Turn)
if((Turn)>MaxAngularSpeed)Turn=MaxAngularSpeed;
end
if(Turn<-MaxAngularSpeed)Turn=-MaxAngularSpeed;
end
TurnMsg.Angular.Z=(Turn)
send(TurnPub,TurnMsg);
s=radtodeg(WantedAngle-CurrentAngle(3));
end
end


