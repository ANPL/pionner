function [ DegVector ] = QtrToDeg( QtrVector )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
w=QtrVector.Pose.Pose.Orientation.W;
z=QtrVector.Pose.Pose.Orientation.Z;
y=QtrVector.Pose.Pose.Orientation.Y;
x=QtrVector.Pose.Pose.Orientation.X;
AngularVector=[w,z,y,x];
DegVector=quat2eul(AngularVector);
end

